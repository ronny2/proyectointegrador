<?php
/**
 * @title: Proyecto integrador Ev01 - Página principal
 * @description:  Bienvenida a la aplicación
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */
  include 'header.php';
?>
    <div class="jumbotron jumbotron-fluid">
        <div id="bienvenida" class="container">       
            <?php 
                  echo "<h1 class='display-3'>Bienvenid@ a $appname</h1>";

                  if ($loggedin) echo "<span class='badge badge-default'> Has iniciado sesión: ".$user."</span>";
                  else           echo "<span class='badge badge-default'> por favor, regístrate o inicia sesión.</span>";
              ?>
        </div>
     </div>
    <div id="bienvenida" class="img-fluid" alt="Responsive image"> 
        <div class="row">
                <div class="offset-md-3 col-md-6">
                   <img src="https://thumbs.dreamstime.com/b/cafe-shop-exterior-street-restraunt-building-vector-illustration-flat-style-97693303.jpg"  alt="" title=""/>
                
                   <p class="lead">BIENVENIDOS AL MEJOR COFFE SHOP DE LA CIUDAD </p>   
              </div>
        </div>                
      </div>
    <footer class="footer">
      <div class="container">
        <span class="text-muted">Desarrollo Web - 2º DAM.</span>
      </div>
    </footer>
  </body> 
</html>